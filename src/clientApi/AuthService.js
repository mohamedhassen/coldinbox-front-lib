import HttpService from '../utils/HttpService'

export default class AuthService {
    static logIn = (username, password) =>
        HttpService.post('/auth/login', { username, password })

    static signUp = (email, password) =>
        HttpService.post('/auth/sign-up', {
            email,
            password,
        })

    static sendResetPasswordEmail = (email) =>
        HttpService.post('/auth/send-reset-password-email', {
            email,
        })

    static setNewPassword = (password, tmp_tk) =>
        HttpService.post('/auth/set-new-password', {
            password,
            tmp_tk,
        })
}
