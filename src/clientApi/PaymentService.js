import HttpService from '../utils/HttpService'

export default class PaymentService {
    static fetchPublicKey = () => HttpService.get('/payment/public-key')

    static subscribeCustomer = (paymentMethod, plan, userId) =>
        HttpService.post('/payment/subscribe', {
            paymentMethod,
            plan,
            userId,
        })

    static applyCoupon = (userId, couponId) =>
        HttpService.post('/payment/apply-coupon', {
            userId,
            couponId,
        })
}
