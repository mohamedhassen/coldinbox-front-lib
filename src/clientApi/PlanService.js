import HttpService from '../utils/HttpService'

export default class PlanService {
    static getAllPlans = () => HttpService.get(`/plans`)
}
