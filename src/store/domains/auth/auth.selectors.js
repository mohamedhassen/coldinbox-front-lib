import { createSelector } from 'reselect'

export const selectAccessToken = store => store.user.access_token

export const isAuthenticated = createSelector(selectAccessToken, tk => !!tk)
