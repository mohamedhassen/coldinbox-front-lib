import { createSelector } from 'reselect'
import some from 'lodash/some'
import { PAUSE } from './event.types'

export const selectEvents = store => store.event.events

export const hasNeverLoggedPauseEvents = createSelector(
    selectEvents,
    events => !some(events, { type: PAUSE })
)
