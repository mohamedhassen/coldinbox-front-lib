import {
    SET_PROFILES,
    SET_HAS_CONTACTED_PROFILE,
    LAUNCH_NEW_CAMPAIGN,
    SET_CAMPAIGN_STATUS_FINISHED,
    SET_ALL_CAMPAIGNS_STATUS_STOPPED,
    SET_HAS_LOGGED_PROFILE,
    SET_PROFILE_PERSONAL_INFO,
} from './campaign.actionTypes'

export const launchNewCampaign = ({ newCampaign }) => (dispatch, _) =>
    dispatch({
        type: LAUNCH_NEW_CAMPAIGN,
        payload: { newCampaign },
    })

export const setProfiles = ({ profiles, campaignId }) => (dispatch, _) =>
    dispatch({
        type: SET_PROFILES,
        payload: { profiles, campaignId },
    })

export const setHasContactedProfile = ({ profile, campaignId }) => (
    dispatch,
    _
) =>
    dispatch({
        type: SET_HAS_CONTACTED_PROFILE,
        payload: { profile, campaignId },
    })

export const setHasLoggedProfile = ({ profile, campaignId }) => (dispatch, _) =>
    dispatch({
        type: SET_HAS_LOGGED_PROFILE,
        payload: { profile, campaignId },
    })

export const setProfileInfo = ({ campaignId, profileUrl, profileInfo }) => (
    dispatch,
    _
) =>
    dispatch({
        type: SET_PROFILE_PERSONAL_INFO,
        payload: { campaignId, profileUrl, profileInfo },
    })

export const finishCampaign = ({ campaignId }) => (dispatch, _) =>
    dispatch({
        type: SET_CAMPAIGN_STATUS_FINISHED,
        payload: { campaignId },
    })

export const stopAllCampaign = () => (dispatch, _) =>
    dispatch({
        type: SET_ALL_CAMPAIGNS_STATUS_STOPPED,
    })
