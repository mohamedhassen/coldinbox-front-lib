import { useSelectorWithUpdate } from '../../../utils/hooks'
import {
    selectSearchUrl,
    selectMessage,
    selectNonContactedProfiles,
} from './campaign.selectors'
import { SET_MESSAGE, SET_SEARCH_URL } from './campaign.actionTypes'

export const useMessage = useSelectorWithUpdate(selectMessage, SET_MESSAGE)

export const useSearchUrl = useSelectorWithUpdate(
    selectSearchUrl,
    SET_SEARCH_URL
)

export const useNonContacted = useSelectorWithUpdate(selectNonContactedProfiles)
