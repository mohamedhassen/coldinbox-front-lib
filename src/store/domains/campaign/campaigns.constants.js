export const PROFILE_STATUS = {
    CREATED: 'CREATED',
    CONTACTED: 'CONTACTED',
}

export const CAMPAIGN_STATUS = {
    CREATING: 'CREATING',
    RUNNING: 'RUNNING',
    STOPPED: 'STOPPED',
    FINISHED: 'FINISHED',
}

export const MESSAGE_VARIABLES = {
    FULL_NAME: 'fullName',
    FIST_NAME: 'firstName',
    LAST_NAME: 'lastName',
    // COMPANY: 'company',
    // JOB: 'job',
}
