import { useSelectorWithUpdate } from '../../../utils/hooks'
import {
    selectCurrentPlanMessageQuota,
    selectUser,
    selectDefaultMessage,
} from './user.selectors'
import { fetchUserById } from './user.actionCreators'
import { useFetchOnce } from '../../../utils/useFetchOnce'
import { SET_DEFAULT_MESSAGE } from './user.actionTypes'

export const useMessageQuota = useSelectorWithUpdate(
    selectCurrentPlanMessageQuota
)

export const useDefaultMessage = useSelectorWithUpdate(
    selectDefaultMessage,
    SET_DEFAULT_MESSAGE
)

export const useFetchUser = useFetchOnce(fetchUserById, selectUser)
