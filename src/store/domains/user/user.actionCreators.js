import { UPDATE_USER } from './user.actionTypes'
import UserService from '../../../clientApi/UserService'

export const decrementQuotaUsage = () => async (dispatch, _) => {
    const user = await UserService.decrementQuota()

    updateUser(user)(dispatch, _)
}

export const updateUser = user => (dispatch, _) =>
    dispatch({
        type: UPDATE_USER,
        payload: user,
    })

export const fetchUserById = userId => async (dispatch, _) => {
    const user = await UserService.findById(userId)

    updateUser(user)(dispatch, _)
}

export const fetchFreshProfile = () => async (dispatch, _) => {
    const user = await UserService.getFreshestProfile()

    updateUser(user)(dispatch, _)
}
