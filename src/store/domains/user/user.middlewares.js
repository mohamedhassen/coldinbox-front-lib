import { DECREMENT_QUOTA_USAGE } from './user.actionTypes'
import UserService from '../../../clientApi/UserService'
import { updateUser } from './user.actionCreators'
import { invokeActionCreator } from '../../../utils/reduxUtils'

const notifyServerForQuotaDecrease = store => next => async action => {
    switch (action.type) {
        case DECREMENT_QUOTA_USAGE: {
            try {
                const user = await UserService.decrementQuota()

                invokeActionCreator(store)(updateUser)(user)
            } catch (err) {
                console.error(err)
            }
        }
    }
    return next(action)
}

export default [notifyServerForQuotaDecrease]
