import { INCREMENT_APP_VERSION } from './meta.actionTypes'

export const incrVersion = () => (dispatch, _) =>
    dispatch({
        type: INCREMENT_APP_VERSION,
    })
