import PlanService from '../../../clientApi/PlanService'
import { SET_PLANS, SET_CURRENT_PLAN } from './plan.actionTypes'

export const fetchAndSelectPlanById = (planId) => async (dispatch, _) => {
    await fetchAllPlans()(dispatch, _)

    dispatch({
        type: SET_CURRENT_PLAN,
        payload: planId,
    })
}

export const fetchAllPlans = () => async (dispatch, _) => {
    const plans = await PlanService.getAllPlans()

    dispatch({
        type: SET_PLANS,
        payload: plans,
    })
}
