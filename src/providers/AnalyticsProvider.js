import { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'

function usePageViews(shouldLoadLib = true) {
    const location = useLocation()
    const [libLoaded, setLibLoaded] = useState(!shouldLoadLib)

    useEffect(() => {
        if (!libLoaded) {
            setLibLoaded(true)
        } else {
            return
        }

        /* eslint-disable no-unused-expressions */
        ;(function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r
            ;(i[r] =
                i[r] ||
                function () {
                    ;(i[r].q = i[r].q || []).push(arguments)
                }),
                (i[r].l = 1 * new Date())
            ;(a = s.createElement(o)), (m = s.getElementsByTagName(o)[0])
            a.async = 1
            a.src = g
            m.parentNode.insertBefore(a, m)
        })(
            global,
            global.document,
            'script',
            'https://www.google-analytics.com/analytics.js',
            'ga'
        )
        /* eslint-enable */

        if (global.ga) {
            global.ga(
                'create',
                process.env.NODE_ENV === 'production'
                    ? 'UA-159051444-1'
                    : 'UA-159051444-2',
                'auto'
            )
            global.ga('set', 'checkProtocolTask', null)
        }
    }, [libLoaded])

    useEffect(() => {
        if (global.ga) {
            global.ga('send', 'pageview', location.pathname)
        }
    }, [location])
}

export default function AnalyticsProvider({ children, shouldLoadLib }) {
    usePageViews(shouldLoadLib)

    return children
}
