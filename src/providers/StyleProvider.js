import React from 'react'
import { Reset } from 'styled-reset'
import { ThemeProvider } from 'styled-components'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'

export const COLOR = {
    color_1: '#509CF1',
    color_2: '#EDF5FE',
    color_3: '#247BA0',
    color_3_hover: '#63a1bb',
    color_4: '#3696bf',
    color_5: '#70C1B3',
    color_6: '#B2DBBF',
    color_7: '#eeeced',
    color_8: '#eeeced',
    color_9: '#676767',
    color_10: '#f8cb47',
}

export default function StyleProvider({ children }) {
    const theme = createMuiTheme({
        palette: {
            primary: {
                main: COLOR.color_1,
            },
            secondary: {
                main: COLOR.color_3,
            },
        },
    })

    return (
        <ThemeProvider theme={COLOR}>
            <MuiThemeProvider theme={theme}>
                <Reset />
                {children}
            </MuiThemeProvider>
        </ThemeProvider>
    )
}
