import React from 'react'
import { Elements } from '@stripe/react-stripe-js'
import { loadStripe } from '@stripe/stripe-js'
import PaymentService from '../clientApi/PaymentService'

const stripePromise = PaymentService.fetchPublicKey().then(result =>
    loadStripe(result.payment_public_key)
)

export default function PaymentProvider({ children }) {
    return <Elements stripe={stripePromise}>{children}</Elements>
}
