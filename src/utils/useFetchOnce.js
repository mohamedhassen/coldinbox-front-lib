import { useSelector } from 'react-redux'
import { useEffect } from 'react'
import isEmpty from 'lodash/isEmpty'
import identity from 'lodash/identity'
import { useAction } from './hooks'

/* eslint-disable react-hooks/rules-of-hooks */

export const useFetchOnce = (
    action,
    selector = identity,
    shouldTestEmptiness = false
) => (...params) => {
    // FIXME: this creates an other object params which means useEffect() is called infinitely
    const execAction = useAction(action)
    const selectedData = useSelector(selector)

    useEffect(() => {
        if (shouldTestEmptiness) {
            if (isEmpty(selectedData)) {
                execAction(...params)
            }
        } else {
            execAction(...params)
        }
    }, [])

    return selectedData
}

/* eslint-enable */
