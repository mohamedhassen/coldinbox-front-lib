import ImmutableArray from './ImmutableArray'
import { test } from 'shelljs'

describe('ImmutableArray', () => {
    describe('push', () => {
        test('Pushing to an array should create a new array with the elem', () => {
            const arr = [{ v: 1 }, { v: 2 }]
            const newElm = { v: 3 }

            const newArr = ImmutableArray.push(arr, newElm)
            expect(arr === newArr).toBe(false)
            expect(newArr.length === 3)
            expect(newArr[2] === newElm)
        })
    })

    describe('replace', () => {
        test('We should be able to replace an elem at the head of the array', () => {
            const arr = [{ v: 1 }, { v: 2 }, { v: 3 }]
            const newElm = { v: 1, status: 'ok' }

            const newArr = ImmutableArray.replace(arr, 0, newElm)
            expect(arr === newArr).toBe(false)
            expect(newArr.length === 3)
            expect(newArr[0].status === 'ok')
        })

        test('We should be able to replace an elem in the array', () => {
            const arr = [{ v: 1 }, { v: 2 }, { v: 3 }]
            const newElm = { v: 3, status: 'ok' }

            const newArr = ImmutableArray.replace(arr, 1, newElm)
            expect(arr === newArr).toBe(false)
            expect(newArr.length === 3)
            expect(newArr[1].status === 'ok')
        })

        test('We should be able to replace an elem at the tail of the array', () => {
            const arr = [{ v: 1 }, { v: 2 }, { v: 3 }]
            const newElm = { v: 3, status: 'ok' }

            const newArr = ImmutableArray.replace(arr, 2, newElm)
            expect(arr === newArr).toBe(false)
            expect(newArr.length === 3)
            expect(newArr[2].status === 'ok')
        })
    })
})
