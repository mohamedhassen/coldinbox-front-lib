export default class ImmutableArray {
    static push = (arr = [], elm) => [...arr, elm]

    static replace = (arr, idx, elm) => [
        ...arr.slice(0, idx),
        elm,
        ...arr.slice(idx + 1),
    ]
}
