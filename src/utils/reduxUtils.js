export const bindSelector = selector => props =>
    selector(global.store.getState(), props)

export const bindActionCreator = actionCreator => (...args) =>
    actionCreator(...args)(global.store.dispatch, global.store.getState)

export const invokeActionCreator = store => actionCreator => (...args) =>
    actionCreator(...args)(store.dispatch, store.getState)
