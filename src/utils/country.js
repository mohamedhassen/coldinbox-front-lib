import countryLookup from 'country-code-lookup'
import startCase from 'lodash/startCase'
import toLower from 'lodash/toLower'
import trim from 'lodash/trim'

export const getISOCountryCode = (rawCountry) => {
    const normalizeCountryName = startCase(toLower(trim(rawCountry)))

    if (!normalizeCountryName) return null

    let retainedCountry

    switch (normalizeCountryName) {
        case 'Usa':
            retainedCountry = 'United States'
            break
        case 'Uk':
            retainedCountry = 'United Kingdom'
            break
        case 'Uae':
        case 'Emirates':
            retainedCountry = 'United Arab Emirates'
            break
        default:
            retainedCountry = normalizeCountryName
    }

    const matchedCountry = countryLookup.byCountry(retainedCountry)

    if (!matchedCountry) return null

    return matchedCountry.iso2
}
