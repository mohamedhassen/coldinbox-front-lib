export const ONE_SECOND = 1000
export const ONE_MINUTE = 60 * ONE_SECOND

export const STORE_SYNC_THROTTLE = 200

export const wait = (waitTime) =>
    new Promise((resolve) => setTimeout(resolve, waitTime))

export const waitUntil = (condition) =>
    new Promise((resolve) => {
        const intervalId = setInterval(async () => {
            if (await condition()) {
                clearInterval(intervalId)
                resolve()
            }
        }, 150)
    })
