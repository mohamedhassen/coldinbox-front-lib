export const log = (context) => (message) =>
    console.log(`CONTEXT=${JSON.stringify(context)} ---- ${message}`)
