import React from 'react'
import styled from 'styled-components'
import WhitePageContentBody from './WhitePageContentBody'

const MainCtn = styled.div`
    display: flex;
    width: 100%;
    height: 100%;
    flex-direction: column;
    align-items: center;
`
const BodyCtn = styled.div`
    height: 85%;
    width: 100%;
    background: ${props => props.theme.color_2};
    display: flex;
    align-items: center;
    flex-direction: column;
`
const LogoCtn = styled.div`
    position: relative;
    top: -20px;
`
const ContentCtn = styled.div`
    position: relative;
    top: -40px;
`

const FooterCtn = styled.div`
    height: 15%;
    width: 100%;
    background: ${props => props.theme.color_1};
    display: flex;
    align-items: center;
    flex-direction: column;
    justify-content: center;
`

const LogoImage = styled.img`
    display: inline-block;
    height: 150px;
`
export default function AuthLayout({ children, footer }) {
    return (
        <MainCtn>
            <BodyCtn>
                <LogoCtn>
                    <LogoImage src={'/img/logo.png'} />
                </LogoCtn>
                <ContentCtn>
                    <WhitePageContentBody style={{ width: 400 }}>
                        {children}
                    </WhitePageContentBody>
                </ContentCtn>
            </BodyCtn>
            <FooterCtn>{footer}</FooterCtn>
        </MainCtn>
    )
}
