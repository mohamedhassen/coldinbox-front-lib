import React from 'react'
import styled from 'styled-components'

const SyledErrorMessage = styled.span`
    font-size: 12px;
    color: red;
    font-weight: bold;
`
export default function ErrorMessage({ error }) {
    return <SyledErrorMessage>{error}</SyledErrorMessage>
}
