import styled from 'styled-components'

const WhitePageContentBody = styled.div`
    margin: 0 20px;
    padding: 10px 20px;
    background: white;
    border: 1px ${(props) => props.theme.color_7} solid;
    height: 90%;
    border-radius: 6px;
`

export default WhitePageContentBody
