import React from 'react'
import styled from 'styled-components'
import { useForm } from 'react-hook-form'
import AuthButton from './AuthButton'
import TextField from './TextField'
import { applyCoupon } from '../store/domains/payment/payment.actionCreators'
import { useAction } from '../utils/hooks'
import { useAlert } from 'react-alert'

const StyledForm = styled.form`
    display: flex;
    flex-align: center;
    justify-content: center;
`
export default function CouponWidget() {
    const alert = useAlert()
    const { handleSubmit, register, errors, setError, clearError } = useForm({
        mode: 'onSubmit',
    })
    const applyCouponAct = useAction(applyCoupon)

    const onSubmit = async (formValues) => {
        try {
            const user = await applyCouponAct(formValues.couponId)

            alert.success(
                `You just unlocked ${user.coupon.percent_off}% promotion on all plans !`
            )

            return clearError('couponId')
        } catch (err) {
            return setError('couponId', 'notMatch', err.message)
        }
    }

    return (
        <StyledForm onSubmit={handleSubmit(onSubmit)}>
            <TextField
                height="25%"
                name="couponId"
                error={!!errors.couponId}
                label={
                    !errors.couponId
                        ? 'Do you have a coupon ?'
                        : errors.couponId.message
                }
                inputRef={register({
                    required: 'Enter a coupon code',
                })}
                style={{ marginRight: 10, background: 'white' }}
                variant="filled"
            />
            <AuthButton type="submit">Apply coupon</AuthButton>
        </StyledForm>
    )
}
