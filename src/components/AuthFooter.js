import React from 'react'
import styled from 'styled-components'

const MainCtn = styled.div`
    display: flex;
    width: 40%;
    justify-content: space-around;
    align-items: center;
`

const LabelCtn = styled.div`
    color: white;
`
const ButtonCtn = styled.div`
    margin-left: 10px;
`

export default function AuthFooter({ label, button }) {
    return (
        <MainCtn>
            <LabelCtn>{label}</LabelCtn>
            <ButtonCtn>{button}</ButtonCtn>
        </MainCtn>
    )
}
