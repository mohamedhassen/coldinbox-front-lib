import React from 'react'
import MuiTextField from '@material-ui/core/TextField'
import { withStyles } from '@material-ui/core'

const StyledTextField = withStyles({
    root: {},
})(MuiTextField)

export default function TextField(props) {
    return <StyledTextField size="small" variant="outlined" {...props} />
}
