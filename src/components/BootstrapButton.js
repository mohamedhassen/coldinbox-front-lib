import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'

const StyledBootstrapButton = withStyles({
    root: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
        borderWidth: '2px',
        padding: '1px 20px',
        '&:hover': {
            borderWidth: '2px',
        },
    },
})(Button)

export default function BootstrapButton({ children, ...props }) {
    return <StyledBootstrapButton {...props}>{children}</StyledBootstrapButton>
}
