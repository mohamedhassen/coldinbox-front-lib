import React from 'react'
import styled from 'styled-components'

const StyledButton = styled.button`
    font-size: 20px;
    border-radius: 8px;
    border: 1px ${props => props.theme.color_7} solid;
    padding: 5px 10px;
    background: white;
    color: ${props => props.theme.color_3};
    font-weight: bold;
    cursor: pointer;

    &:hover {
        color: ${props => props.theme.color_4};
    }
`

export default function PrimaryButton({ onClick, children, ...buttonProps }) {
    return (
        <StyledButton onClick={onClick} {...buttonProps}>
            {children}
        </StyledButton>
    )
}
